//
//  VideoAVPlayerViewController.h
//  practicaPeliculas
//
//  Created by sebastian sierra on 9/17/15.
//  Copyright (c) 2015 sebastian sierra. All rights reserved.
//

#import <AVKit/AVKit.h>

@interface VideoAVPlayerViewController : AVPlayerViewController

@end
