//
//  main.m
//  practicaPeliculas
//
//  Created by sebastian sierra on 9/17/15.
//  Copyright (c) 2015 sebastian sierra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
