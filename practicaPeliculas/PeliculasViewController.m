//
//  PeliculasViewController.m
//  practicaPeliculas
//
//  Created by Marlon David Ruiz Arroyave on 19/09/15.
//  Copyright (c) 2015 sebastian sierra. All rights reserved.
//

#import "PeliculasViewController.h"
#import "Peliculas.h"
#import "PeliculaCell.h"

@interface PeliculasViewController ()

@end
NSString * const PELICULA_CELL_IDENTIFIER = @"PeliculaCell";
@implementation PeliculasViewController{
    NSArray *data;
    Peliculas *pelicula;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    data = [Peliculas MoviesArray];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
        
    

}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PeliculaCell *cell = (PeliculaCell *)[tableView dequeueReusableCellWithIdentifier:PELICULA_CELL_IDENTIFIER];
    /*
    if (!cell) {
        cell = [PeliculaCell alloc];
    }
     UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:PELICULA_CELL_IDENTIFIER];
     
     if (!cell) {
     cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:PELICULA_CELL_IDENTIFIER];
     }
    */
    
    pelicula = data[indexPath.row];
    
    cell.title.text = pelicula.title;
    cell.date.text = pelicula.date;
    cell.category.text = pelicula.categoy;
    cell.movieImg.image =[UIImage imageNamed:pelicula.img];
    
    /*
     @property (weak, nonatomic) IBOutlet UILabel *title;
     @property (weak, nonatomic) IBOutlet UILabel *date;
     @property (weak, nonatomic) IBOutlet UILabel *category;
     @property (weak, nonatomic) IBOutlet UIImageView *movieImg;
     */
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self performSegueWithIdentifier:@"Player" sender:self.view];
    
}


@end
