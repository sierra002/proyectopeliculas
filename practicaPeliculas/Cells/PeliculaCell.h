//
//  PeliculaCell.h
//  practicaPeliculas
//
//  Created by centro docente de computos on 11/13/15.
//  Copyright (c) 2015 sebastian sierra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PeliculaCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *title;
@property (strong, nonatomic) IBOutlet UILabel *date;
@property (strong, nonatomic) IBOutlet UILabel *category;
@property (strong, nonatomic) IBOutlet UIImageView *movieImg;


@end
