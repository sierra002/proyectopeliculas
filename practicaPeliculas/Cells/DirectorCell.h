//
//  DirectorCell.h
//  practicaPeliculas
//
//  Created by centro docente de computos on 11/13/15.
//  Copyright (c) 2015 sebastian sierra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DirectorCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *nombre;
@property (strong, nonatomic) IBOutlet UILabel *residencia;
@property (strong, nonatomic) IBOutlet UILabel *fecha;
@property (strong, nonatomic) IBOutlet UIImageView *imgDirector;


@end
