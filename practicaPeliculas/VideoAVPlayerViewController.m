//
//  VideoAVPlayerViewController.m
//  practicaPeliculas
//
//  Created by sebastian sierra on 9/17/15.
//  Copyright (c) 2015 sebastian sierra. All rights reserved.
//

#import "VideoAVPlayerViewController.h"
#import <AVFoundation/AVFoundation.h>

@interface VideoAVPlayerViewController ()

@end

@implementation VideoAVPlayerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.player play];
    self.player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
