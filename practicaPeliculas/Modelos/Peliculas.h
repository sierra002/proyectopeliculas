//
//  Peliculas.h
//  practicaPeliculas
//
//  Created by centro docente de computos on 11/13/15.
//  Copyright (c) 2015 sebastian sierra. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Peliculas : NSObject
- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

+ (NSArray *)MoviesArray;

@property (weak, nonatomic) NSString *title;
@property (weak, nonatomic) NSString *date;
@property (weak, nonatomic) NSString *categoy;
@property (weak, nonatomic) NSString *img;
@property (strong, nonatomic) NSURL *video;


@end
