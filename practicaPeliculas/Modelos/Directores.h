//
//  Directores.h
//  practicaPeliculas
//
//  Created by centro docente de computos on 11/13/15.
//  Copyright (c) 2015 sebastian sierra. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Directores : NSObject

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

+ (NSArray *)DirectoresArray;

@property (assign, nonatomic) NSInteger *directorId;
@property (weak, nonatomic) NSString *name;
@property (weak, nonatomic) NSString *residence;
@property (weak, nonatomic) NSString *bornDate;
@property (weak, nonatomic) NSString *img;

@end
