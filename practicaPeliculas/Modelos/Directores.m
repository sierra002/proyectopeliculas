//
//  Directores.m
//  practicaPeliculas
//
//  Created by centro docente de computos on 11/13/15.
//  Copyright (c) 2015 sebastian sierra. All rights reserved.
//

#import "Directores.h"

@implementation Directores

- (instancetype)initWithDictionary:(NSDictionary *)dictionary{
    
    if (self = [super init]) {
        self.name = dictionary[@"name"];
        self.residence = dictionary[@"residence"];
        self.bornDate = dictionary[@"bornDate" ];
        self.img = (dictionary[@"img" ])?dictionary[@"img" ]:@"";
    }
    
    return self;
    

}



+ (NSArray *)DirectoresArray{
    
    NSMutableArray * directores = [NSMutableArray array];
    
    NSArray *array = @[
                       
                       @{
                           @"name": @"Alejandro Gonzales",
                           @"residence":@"avenida siemre viva",
                           @"bornDate":@"2015/11/30",
                           @"img":@"Alejandro Gonzales.jpg"
                           },
                       
                       @{
                           @"name": @"Gore_Verbinski",
                           @"residence":@"avenida siemre viva",
                           @"bornDate":@"2015/11/30",
                           @"img":@"Gore_Verbinski.jpg"
                           },
                       @{
                           @"name": @"Spielberg",
                           @"residence":@"avenida siemre viva",
                           @"bornDate":@"2015/11/30",
                           @"img":@"Spielberg.jpg"
                           }
                       
                       ];
    
    for (NSDictionary *dict in array) {
        Directores *director = [[Directores alloc]initWithDictionary:dict];
        [directores addObject:director];
    }
    
    return directores.copy;
}

@end
