//
//  Peliculas.m
//  practicaPeliculas
//
//  Created by centro docente de computos on 11/13/15.
//  Copyright (c) 2015 sebastian sierra. All rights reserved.
//

#import "Peliculas.h"

@implementation Peliculas


/*
 @property (weak, nonatomic) NSString *title;
 @property (weak, nonatomic) NSString *date;
 @property (weak, nonatomic) NSString *categoy;
 @property (weak, nonatomic) NSString *img;
 @property (strong, nonatomic) NSURL *video;
 */
- (instancetype)initWithDictionary:(NSDictionary *)dictionary{
    
    if (self = [super init]) {
        self.title = dictionary[@"title"];
        self.date= dictionary[@"date"];
        self.categoy = dictionary[@"categoy" ];
        self.img = (dictionary[@"img" ])?dictionary[@"img" ]:@"";
        self.video = dictionary[@"video"];
    }
    
    return self;
    
    
}



+ (NSArray *)MoviesArray{
    
    NSMutableArray * peliculasArray = [NSMutableArray array];
    
    NSArray *array = @[
                       
                       @{
                           @"title": @"Avengers",
                           @"date":@"2015/11/30",
                           @"categoy":@"accion",
                           @"img":@"avengers.jpg",
                           @"video":@""
                           },
                       
                       @{
                           @"title": @"El Hobit",
                           @"date":@"2015/11/30",
                           @"categoy":@"Fantasia",
                           @"img":@"hobbit.png",
                           @"video":@""
                           },

                       @{
                           @"title": @"La Momia",
                           @"date":@"2015/11/30",
                           @"categoy":@"Accion",
                           @"img":@"La_Momia_Regresa.jpg",
                           @"video":@""
                           },

                       
                       ];
    
    for (NSDictionary *dict in array) {
        Peliculas *pelicula = [[Peliculas alloc]initWithDictionary:dict];
        [peliculasArray addObject:pelicula];
    }
    
    return peliculasArray.copy;
}

@end
