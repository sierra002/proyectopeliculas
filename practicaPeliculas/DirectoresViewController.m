//
//  DirectoresViewController.m
//  practicaPeliculas
//
//  Created by sala209_02 on 11/20/15.
//  Copyright (c) 2015 sebastian sierra. All rights reserved.
//

#import "DirectoresViewController.h"
#import "Directores.h"
#import "DirectorCell.h"

NSString * const CELL_IDENTIFIER = @"DirectorCell";
@interface DirectoresViewController ()

@end

@implementation DirectoresViewController{
    NSArray *data;
    Directores *director;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    data = [Directores DirectoresArray];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return data.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DirectorCell *cell = [tableView dequeueReusableCellWithIdentifier:CELL_IDENTIFIER];
    
    /*;
     UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CELL_IDENTIFIER];
     
     if (!cell) {
     cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CELL_IDENTIFIER];
     }
    if (!cell) {
        cell = [DirectorCell alloc];
    }*/
    director = data[indexPath.row];
    cell.imgDirector.image = [UIImage imageNamed:director.img];
    cell.nombre.text = director.name;
    cell.residencia.text = director.residence;
    cell.fecha.text = director.bornDate;
    /* cell.imageView.image = [UIImage imageNamed:director.img];
     cell.textLabel.text = director.name;
     cell.detailTextLabel.text = director.residence;*/
    
    
    return cell;
    
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self performSegueWithIdentifier:@"Movies" sender:self.view];
    
}

@end
