//
//  DirectoresViewController.h
//  practicaPeliculas
//
//  Created by sala209_02 on 11/20/15.
//  Copyright (c) 2015 sebastian sierra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DirectoresViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
