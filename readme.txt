Una breve descripción de lo que hace la app

La app consiste en una especie de catalogo donde los usuarios podrán consultar las últimas peliculas 
en cartelera y/o directores favoritos y las peliculas de este.
 
Documento Canvas

https://docs.google.com/drawings/d/16l-czeAnJhsETLCAI0ps-fZEi_JdAlsJCOzcgPAwsDE/edit

Integrantes del equipo.
Sebastián Sierra
Carlos Enrique Gómez

Una lista de las actividades a desarrollar (Backlog)

1.Diseño de la aplicación
2.Desarrollo de la navegabilidad de la aplicación
3.Desarrollo de las listas de peliculas
4.Desarrollo de listas de directores
5.Desarrollo de los detalles de las peliculas
6.Desarrollo de los detalles de los directores


Configurar el primer sprint. Colocar las actividades a desarrollar que serán entregadas el viernes 25 Septiembre de 2015

Para el primer sprint se desarrollarán las vistas de cómo funcionará la app, es decir, navegabilidad y diseño de pantallas.
En las pantallas se incluyen el login, la interfaz donde el usuario podrá consultar películas o directores y por ultimo las pantallas con las respectivas listas.